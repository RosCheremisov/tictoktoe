package com.runner;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Field {

    private final char[][] field;
    private char singOfPlayer;
    private final int sizeField = 3;
    private final char EMPTY = '-';
    private final Scanner scanner = new Scanner(System.in);

    public Field() {
        field = new char[sizeField][sizeField];
        singOfPlayer = 'o';
    }

    public void generateField() {
        for (int i = 0; i < sizeField; i++) {
            for (int j = 0; j < sizeField; j++) {
                field[i][j] = EMPTY;
            }
        }
    }

    public void printField() {
        for (char[] chars : field) {
            for (char aChar : chars) {
                System.out.print(aChar + " ");
            }
            System.out.println();
        }
    }

    public void nextStep() {
        changePlayer();

        int x = -1;
        int y = -1;

        do {
            System.out.println("Игрок " + singOfPlayer + " Выберите позицию от 1 до 3, где первое значение позиция по горизонтали а вторая по вертикали : ");
            try {
                x = nextInt() - 1;
                y = nextInt() - 1;
            } catch (NumberFormatException e) {
                System.out.println("Неверный ввод");
            } catch (InputMismatchException exception) {
                System.out.println(exception.getMessage() + " change input");
                scanner.nextLine();
            }
        } while (!isInputValid(x,y));
        field[x][y] = singOfPlayer;
    }

    boolean isInputValid(int x, int y) {
            if (x < 0 || y < 0 || x >= 3 || y >= 3 ) {
                System.out.println("Неправильно должно быть число от 1 до 3");
                return false;
            }
        if (field[x][y] != '-') {
            System.out.println("Позиция занята");
            return false;
        }
        return true;
    }

    public boolean isGameEnded() {
        return !hasWinner() && !isFieldFull();
    }

    private boolean isFieldFull() {
        for (int i = 0; i < sizeField; i++) {
            for (int j = 0; j < sizeField; j++) {
                if (field[i][j] == EMPTY) {
                    return false;
                }
            }
        }
        System.out.println("Draw");
        return true;
    }

    private boolean hasWinner() {
        for (int i = 0; i < sizeField; i++) {
            for (int j = 0; j < sizeField; j++) {
                if ((field[0][j] == singOfPlayer && field[1][j] == singOfPlayer && field[2][j] == singOfPlayer) ||
                        (field[i][0] == singOfPlayer && field[i][1] == singOfPlayer && field[i][2] == singOfPlayer) ||
                        (field[0][0] == singOfPlayer && field[1][1] == singOfPlayer && field[2][2] == singOfPlayer) ||
                        (field[0][2] == singOfPlayer && field[1][1] == singOfPlayer && field[2][0] == singOfPlayer)) {
                    System.out.println("Победа " + singOfPlayer);
                    return true;
                }
            }
        }
        return false;
    }

    private void changePlayer() {
        if (singOfPlayer == 'x') {
            singOfPlayer = 'o';
        } else {
            singOfPlayer = 'x';
        }
    }

    private int nextInt() {
        if (scanner.hasNextInt()) {
            return scanner.nextInt();
        } else {
            System.out.println("Incorrect input");
            scanner.nextLine();
        }
        return -1;
    }
}

