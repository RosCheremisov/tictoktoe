package com.runner;

public class TicTokToeRunner {

    public static void main(String[] args) {
        Field game = new Field();
        game.generateField();
        game.printField();
        do {
            game.nextStep();
            game.printField();
        } while (!game.isGameEnded());
    }
}